package myprojects.lection3.utils;

import org.openqa.selenium.remote.BrowserType;

public class Properties {

    private static final String DEAFULT_BROWSER = BrowserType.CHROME;

    public static final String BASE_URL = "https://stage.webformslibrary.com/forms/auto-insurance/form_2/?nostepanimation=true";
    public static final String BROWSER = "browser";

    public static String getBaseUrl() {
        return System.getProperty(BASE_URL, BASE_URL);
    }

    public static String getBrowser() {
        return System.getProperty(BROWSER, DEAFULT_BROWSER);
    }
}
