package myprojects.lection3.pages;

import myprojects.lection3.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class FormPage extends BaseTest {
    private WebDriver driver;
    private By chooseFromList;
    private By chooseMakeOfVehicle;
    private By nextButton;
    private By chooseFirstOfSelect;
    private By yesOrNoAnswer;
    private By chooseDate;
    private By zipFields;
    private By streetAddress;
    private By phoneNumber;
    private By requestQuotes;
    private By actualResult;
    private By gender;
    private By email;
    private By firstName;
    private By lastName;


    public FormPage(WebDriver driver) {

        chooseFromList = By.xpath("//div[@class='jsf-btn-group']/span[1]");
        chooseMakeOfVehicle = By.className("jsf-btn-quad-option");
        nextButton = By.xpath("//span[@data-jsf='next']");
        chooseFirstOfSelect = By.xpath("//div[@class='jsf-btn-group jsf-btn-group-column']/span[1]");
        yesOrNoAnswer = By.xpath("//span[.='Yes']");
        chooseDate = By.className("jsf-btn-small");
        zipFields = By.xpath("//input[@data-jsf='Zip']");
        streetAddress = By.xpath("//input[@data-jsf='StreetAddress']");
        phoneNumber = By.xpath("//input[@data-jsf='PrimaryPhone']");
        requestQuotes = By.xpath("//span[text()='Request Quotes!']");
        actualResult = By.xpath("//p[@class='jsf-title-lg']");
        gender = By.xpath("//span[.='Male']");
        email = By.xpath("//input[@type='email']");
        firstName = By.xpath(" //input[@data-jsf='FirstName']");
        lastName = By.xpath(" //input[@data-jsf='LastName']");

        this.driver = driver;
    }

    public FormPage whatYearIsYourVehicle() throws InterruptedException {

        List<WebElement> vehicleYear = driver.findElements(chooseDate); // year of vehicle

        while (vehicleYear.isEmpty()) {

            vehicleYear = driver.findElements(chooseDate);
        }

        vehicleYear.get(returnRandomValue(vehicleYear)).click();

        return this;
    }

    public FormPage whatMakeIsYourVehicle() {

        List<WebElement> makeOfVehicle = driver.findElements(chooseMakeOfVehicle); // year of vehicle

        makeOfVehicle.get(returnRandomValue(makeOfVehicle)).click();

        return this;
    }

    public FormPage whatModelIsYourCar() {

        driver.findElement(chooseFirstOfSelect).click();
        return this;
    }

    public FormPage whatSubmodelIsYourCar() {

        WebDriverWait wait = new WebDriverWait(driver, 15);

        wait.until(ExpectedConditions.visibilityOf(driver.findElement(chooseFirstOfSelect))).click();

        return this;
    }

    public FormPage carDetails() {

        driver.findElement(nextButton).click();
        return this;
    }

    public FormPage annualMileage() {

        driver.findElement(chooseFromList).click();
        return this;
    }

    public FormPage currentlyInsured() {

        driver.findElement(yesOrNoAnswer).click();
        return this;
    }

    public FormPage currentInsuranceProvider() {

        List<WebElement> makeOfVehicle = driver.findElements(chooseMakeOfVehicle);

        makeOfVehicle.get(returnRandomValue(makeOfVehicle)).click();

        return this;
    }

    public FormPage howLongYouHaveBeenContinuouslyInsured() {

        driver.findElement(chooseFromList).click();
        return this;
    }

    public FormPage currentPolicyExpiration() {

        driver.findElement(chooseFromList).click();
        return this;
    }

    public FormPage coveragePreferences() {

        driver.findElement(nextButton).click();
        return this;
    }

    public FormPage whatIsYourBirthYear() {

        driver.findElement(chooseDate).click();
        return this;
    }

    public FormPage whatIsYourBirthMonth() {

        driver.findElement(chooseDate).click();
        return this;
    }

    public FormPage andFinallyYourBirthday() {

        driver.findElement(chooseDate).click();
        return this;
    }

    public FormPage gender() {

        driver.findElement(gender).click();
        return this;
    }

    public FormPage education() {

        driver.findElement(chooseFromList).click();
        return this;
    }

    public FormPage occupation() {

        driver.findElement(chooseFromList).click();
        return this;
    }

    public FormPage areYouMarried() {

        driver.findElement(yesOrNoAnswer).click();
        return this;
    }

    public FormPage areYouHomeowner() {

        driver.findElement(yesOrNoAnswer).click();
        return this;
    }

    public FormPage yourDriverLicenseIs() {

        driver.findElement(chooseFromList).click();
        return this;
    }

    public FormPage sr22Required() {

        if (driver.findElement(yesOrNoAnswer).isDisplayed()) {

            driver.findElement(yesOrNoAnswer).click();
        }
        return this;
    }

    public FormPage fillZipField() {

        driver.findElement(zipFields).sendKeys("90213");
        return this;
    }

    public FormPage fillStreetAddress() {

        driver.findElement(streetAddress).sendKeys("106 8th Avenue S");
        return this;
    }

    public FormPage fillPhoneNumber() throws InterruptedException {

        driver.findElement(phoneNumber).click();

        Thread.sleep(2000);

        driver.findElement(phoneNumber).sendKeys("5236523558");

        Thread.sleep(1000);

        return this;

    }

    public FormPage fillEmail() throws InterruptedException {

        Thread.sleep(1000);

        driver.findElement(email).sendKeys("test@i.ua");
        return this;
    }

    public FormPage fillFirstName() {

        driver.findElement(firstName).sendKeys("Alex");
        return this;
    }

    public FormPage fillLastName() {

        driver.findElement(lastName).sendKeys("Pass");
        return this;
    }

    public FormPage clickNextButton() {

        driver.findElement(nextButton).click();
        return this;
    }


    public FormPage requestQuotesButtonClick() throws InterruptedException {


        driver.findElement(requestQuotes).click();

        Thread.sleep(5000);

        return this;
    }

    public FormPage actualResultVerifiing() {

        String expectedResult = "You\'re done, Alex!";

        WebDriverWait wait = new WebDriverWait(driver, 15);

        String actual = wait.until(ExpectedConditions.visibilityOf(driver.findElement(actualResult))).getText();

        Assert.assertEquals(actual, expectedResult);

        return this;

    }


}
