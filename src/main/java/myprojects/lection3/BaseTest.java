package myprojects.lection3;

import myprojects.lection3.utils.Properties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;


public abstract class BaseTest {

    public static WebDriver getDriver() {
        WebDriver driver;
        String browser = Properties.getBrowser();
        switch (browser) {
            case "firefox":
                System.setProperty("webdriver.gecko.driver", "resources/geckodriver.exe");
                return new FirefoxDriver();
            case "ie":
            case "internet explorer":
                System.setProperty("webdriver.ie.driver", "resources/IEDriverServer.exe");
                return new InternetExplorerDriver();
            case "chrome":
            default:
                System.setProperty("webdriver.chrome.driver", "resources/chromedriver.exe");
                driver = new ChromeDriver();
                driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
                return driver;
        }

    }



    public int returnRandomValue(List<WebElement> elements) {

        int size = elements.size();

        return size <= 1 ? 0 : new Random().nextInt(size - 1);

    }
}
