package myprojects.lection3.tests;

import myprojects.lection3.BaseTest;
import myprojects.lection3.pages.FormPage;
import myprojects.lection3.utils.Properties;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.awt.*;
import java.awt.event.KeyEvent;

public class Auto2 extends BaseTest {
    WebDriver driver;

    @BeforeMethod
    public void setUp() {

        driver = getDriver();


        driver.get(Properties.getBaseUrl());

    }

    @AfterMethod
    public void Close() throws Exception {

        driver.quit();
        Robot rb = new Robot();
        rb.keyPress(KeyEvent.VK_ENTER);
        rb.keyRelease(KeyEvent.VK_ENTER);
    }
    @Test

    public void fillAuto() throws InterruptedException {


        FormPage form = new FormPage(driver);

        form.whatYearIsYourVehicle()
                .whatMakeIsYourVehicle()
                .whatModelIsYourCar()
                .whatSubmodelIsYourCar()
                .carDetails()
                .annualMileage()
                .currentlyInsured()
                .currentInsuranceProvider()
                .howLongYouHaveBeenContinuouslyInsured()
                .currentPolicyExpiration()
                .coveragePreferences()
                .whatIsYourBirthYear()
                .whatIsYourBirthMonth()
                .andFinallyYourBirthday()
                .gender()
                .education()
                .occupation()
                .areYouMarried()
                .areYouHomeowner()
                .yourDriverLicenseIs()
                .sr22Required()
                .fillEmail()
                .fillFirstName()
                .fillLastName()
                .clickNextButton()
                .fillZipField()
                .fillStreetAddress()
                .fillPhoneNumber()
                .requestQuotesButtonClick()
                .actualResultVerifiing();

    }

}
